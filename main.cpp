#include "Threes.h"
#include <ctime>
#include <cmath>
#ifdef _WIN32
#include <conio.h>
#elif defined(__linux__)
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
using namespace std;
int getch(void){
	struct termios oldattr, newattr;
	int ch;
	tcgetattr( STDIN_FILENO, &oldattr );
	newattr = oldattr;
	newattr.c_lflag &= ~( ICANON | ECHO );
	tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
	ch = getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );
	return ch;
}
#endif

#define MAX_DEPTH 6
#define INF 2147483647

const char* dirToStr(dir_e dir){
	if(dir == LEFT)   return "LEFT   ";
	if(dir == DOWN)   return "DOWN   ";
	if(dir == RIGHT)  return "RIGHT  ";
	if(dir == UP)     return "UP     ";
	return "INVALID";
}

// Exmaple1. Original game played with AWSD keys
dir_e getDirFromKeyboard(){
	int dir;
	dir = getch();

	if(dir == 'A' || dir == 'a')  return LEFT;
	if(dir == 'S' || dir == 's')  return DOWN;
	if(dir == 'D' || dir == 'd')  return RIGHT;
	if(dir == 'W' || dir == 'w')  return UP;
	return INVALID;
}

// Exmaple2. Random direction inserting bot
dir_e getRandDir(){
	int dir = rand()%4;

	if(dir == 0)  return LEFT;
	if(dir == 1)  return DOWN;
	if(dir == 2)  return RIGHT;
	if(dir == 3)  return UP;
	return INVALID;
}

void shift(int **A, int dir){
	if(dir == LEFT){
		for(int i = 0;i < 4 ; i++)
			for(int j = 0; j < 3; j++){
				if(A[i][j]!=0){
					if((A[i][j]==1 && A[i][j+1] == 2) || (A[i][j]==2 && A[i][j+1] == 1)){
						A[i][j] += A[i][j+1];
						A[i][j+1] = 0;
			
					}	
					if(A[i][j] >= 3 && A[i][j] == A[i][j+1]){
						A[i][j] += A[i][j+1];
						A[i][j+1] = 0;
					}
				}
				else{
					A[i][j] = A[i][j+1];
					A[i][j+1] = 0;
				}
			}
	}

	if(dir == UP){
		for(int i = 0;i < 4 ; i++)
			for(int j = 0; j < 3; j++){
				if(A[j][i] != 0){
					if((A[j][i]==1 && A[j+1][i] == 2) || (A[j][i]==2 && A[j+1][i] == 1)){
						A[j][i] += A[j+1][i];
						A[j+1][i] = 0;
					}	
					if(A[j][i] >= 3 && A[j][i] == A[j+1][i]){
						A[j][i] += A[j+1][i];
						A[j+1][i] = 0;
					}
				}
				else{
					A[j][i] = A[j+1][i];
					A[j+1][i] = 0;
				}
			}
	}

	if(dir == RIGHT){
		for(int i = 0;i < 4 ; i++)
			for(int j = 3; j > 0; j--){
				if(A[i][j]!=0){
					if((A[i][j]==1 && A[i][j-1] == 2) || (A[i][j]==2 && A[i][j-1] == 1)){
						A[i][j] += A[i][j-1];
						A[i][j-1] = 0;
					}	
					if(A[i][j] >= 3 && A[i][j] == A[i][j-1]){
						A[i][j] += A[i][j-1];
						A[i][j-1] = 0;
					}
				}
				else{
					A[i][j] = A[i][j-1];
					A[i][j-1] = 0;
				}
			}
	}

	if(dir == DOWN){
		for(int i = 0; i < 4 ; i++)
			for(int j = 3; j > 0; j--){
				if(A[j][i]!=0){

					if((A[j][i] == 1 && A[j-1][i] == 2) || (A[j][i]==2 && A[j-1][i] == 1)){
						A[j][i] += A[j-1][i];
						A[j-1][i] = 0;
					}

					if(A[j][i] >= 3 && A[j][i] == A[j-1][i]){
						A[j][i] += A[j-1][i];
						A[j-1][i] = 0;
					}
				}
				else{
					A[j][i] = A[j-1][i];
					A[j-1][i] = 0;
				}
			}
	}
}

bool isWin(int **A, int currentMax){
	for(int i = 0; i < 4; i++)
		for(int j = 0 ; j < 4; j++)
			if(A[i][j] >= currentMax*2)
				return true;
	return false;
}

bool isOver(int **A){
	int dx[4] = {1,-1,0,0};
	int dy[4] = {0,0,1,-1};


	for(int i = 0; i < 4; i++)
		for(int j = 0; j < 4; j++){
			if(A[i][j]==0)
				return false;
			for(int a = 0; a < 4; a++){
				int x = i + dx[a];
				int y = j + dy[a];
				if(x >= 0 && x < 4 && y >= 0 && y < 4 ){
					if(A[i][j] >= 3 && A[i][j] == A[x][y])
						return false;
					if(A[i][j] == 1 && A[x][y] ==2 || A[i][j] == 2 && A[x][y] == 1)
						return false;
				}
			}
		}
	return true;
}

bool isTerminal(int depth, int **A){

	if(depth == 0)
		return true;
	if(isOver(A))
		return true;

}

bool isEqual(int **A, int B[][4]){
	
	for(int i = 0; i < 4; i++)
		for(int j = 0; j < 4; j++)
			if(A[i][j] != B[i][j])
				return false;
	return true;
}

bool isLegal(int **A, int dir){
	
	int back[4][4] = {{0}};
	for(int i = 0; i < 4;i++){
        for(int j = 0; j < 4; j++){
			back[i][j] = A[i][j];
		}
	}
	shift(A,dir);
	if(isEqual(A,back)){
		for(int i = 0; i < 4;i++){
			for(int j = 0; j < 4; j++){
				A[i][j] = back[i][j];
			}
		}
		return false;
	}
	for(int i = 0; i < 4;i++){
		for(int j = 0; j < 4; j++){
			A[i][j] = back[i][j];
		}
	}
	return true;
}
double currentScore(int **A){
	double score = 0;
	for(int i = 0; i < 4;i++)
		for(int j = 0; j < 4;j ++)
			if(A[i][j] > 3)
				score += (log(A[i][j]/3)/log(2));
	return score;
}

double evaluate(int **A){

	int c = 0;
	int x[8] =  {0, 0, 3, 3, 0, 0, 3, 3};
	int y[8] =  {0, 3, 0, 3, 0, 3, 0, 3};
	int dx[8] =  {1, 1,-1,-1, 1, 1,-1,-1};
	int dy[8] =  {1,-1, 1,-1, 1, -1,1,-1};


	double weightValue[8] = {0};
	double weight = 1;
	double commonRatio = 0.25;
	for(int k = 0; k < 4; k++){
		c = 0;
		weight = 1;
		int i = x[k]; int j = y[k];
		int y = dy[k]; int x = dx[k];
		while(c <= 15){
			if(j==4 || j == -1){
				i += x;
				y = y * -1;
				j += y;
			}
			weightValue[k] += A[i][j]*weight;
			weight *= commonRatio;
			j += y;
			c++;
		}
		//~ weightValue[k] += 0.1*currentScore(A);
	}

	for(int k = 4; k < 8; k++){
		c = 0;
		weight = 1;
		int i = x[k]; int j = y[k];
		int y = dy[k]; int x = dx[k];
		while(c <= 15){
			if(i==4 || i == -1){
				x= x * -1;
				i += x;
				j+=y;
			}
			weightValue[k] += A[i][j]*weight;
			weight *= commonRatio;
			i += x;
			c++;
		}
		//~ weightValue[k] += 0.1*currentScore(A);
	}
	double maxVal = weightValue[1];
	for(int k = 1; k < 8; k++)
		if(weightValue[k] > maxVal)
			maxVal = weightValue[k];
			
	
	return maxVal;

}


double minValue(int depth, int playerIndex, int **A, double alpha, double beta, int newLocation[4][4],char hint);

double maxValue(int depth, int playerIndex, int **A, double alpha, double beta){

	int Back[4][4] = {{0}};
	int newLocation[4][4] = {{0}};
	int currentMax = 0;
	if(isTerminal(depth, A))  return evaluate(A);
	
    double v = -1 * INF;

    for(int i = 0; i < 4;i++)
        for(int j = 0; j < 4; j++)
            Back[i][j] = A[i][j];

    for(int action = 0; action < 4; action++){
		
		if(!isLegal(A, action)) continue;
		
		for(int i = 0; i < 4; i++)
			for(int j = 0; j < 4; j++)
				if(A[i][j] > currentMax)
					currentMax = A[i][j];
		
		if(currentMax < 192)
			currentMax = 192;
		
		if(isWin(A, currentMax))
			return 1000000;
        
		shift(A, action);	
		 
		if(action == LEFT){
			for(int i = 0; i < 4;i++)
				for(int j = 0; j < 4; j++)
					if(A[i][j] != Back[i][j])
						if(A[i][3] == 0)
							newLocation[i][3] = 1;
		}
		
		if(action == RIGHT){
			for(int i = 0; i < 4;i++)
				for(int j = 0; j < 4; j++)
					if(A[i][j] != Back[i][j])
						if(A[i][0] == 0)
							newLocation[i][0] = 1;
		}
		
		if(action == UP){
			for(int i = 0; i < 4;i++)
				for(int j = 0; j < 4; j++)
					if(A[i][j] != Back[i][j])
						if(A[3][j] == 0)
							newLocation[3][j] = 1;
		}
		
		if(action == DOWN){
			for(int i = 0; i < 4;i++)
				for(int j = 0; j < 4; j++)
					if(A[i][j] != Back[i][j])
						if(A[0][j] == 0)
							newLocation[0][j] = 1;
		}
		
					
		
		double score = evaluate(A);
		double tmp = minValue(depth - 1,1-playerIndex, A, alpha, beta, newLocation,0);		
		
		score += tmp*pow(0.9, MAX_DEPTH - depth+1);

        v = std::max (v, score);
        
        if(v >= beta) return v;
        alpha = std::max(alpha, v);
        
        for(int i = 0; i < 4;i++)
			for(int j = 0; j < 4; j++){
				A[i][j] = Back[i][j];
				newLocation[i][j] = 0;
			}
    }
    return v;
}

double minValue(int depth, int playerIndex, int **A, double alpha, double beta, int newLocation[4][4], char hint){

	if(isTerminal(depth, A))  return evaluate(A);
	
    double v = INF;
    int currentMax = 0;

    int Back[4][4] = {{0}};
    int emptyX[16] = {0};
    int emptyY[16] = {0};

    for(int i = 0; i < 4;i++)
        for(int j = 0; j < 4; j++){
            Back[i][j] = A[i][j];
            if(A[i][j] > currentMax)
				currentMax = A[i][j];
        }

    int c = 0;
    for(int i = 0; i < 4;i++)
        for(int j = 0; j < 4; j++)
        if(newLocation[i][j] == 1){
            emptyX[c] = i;
            emptyY[c] = j;
            c++;
    }
	
    for(int i = 0; i < c; i++){
		double score = 0;
		if(hint != 0){
			if(hint != '+'){
				A[emptyX[i]][emptyY[i]] = hint -'0';
				//score += evaluate(A);
				score += maxValue(depth - 1, 1-playerIndex, A, alpha, beta);
				for(int x = 0; x < 4;x++)
						for(int y = 0; y < 4; y++)
							A[x][y] = Back[x][y];
			}
			else{
				int num = currentMax/8 + 3;
				for(int n = 1; n <=3 ;n ++){
					A[emptyX[i]][emptyY[i]] = n;

					//score += evaluate(A) * 1.0 / num;
					score += (1.0/num) * maxValue(depth - 1, 1-playerIndex, A, alpha, beta);
					for(int x = 0; x < 4;x++)
						for(int y = 0; y < 4; y++)
							A[x][y] = Back[x][y];
				}
				
				for(int n = 1; n <= currentMax / 48; n++){
					//score += evaluate(A) * 1.0 / num;
					A[emptyX[i]][emptyY[i]] = n*6;
					score += (1.0/num) * maxValue(depth - 1, 1-playerIndex, A, alpha, beta);
					for(int x = 0; x < 4;x++)
						for(int y = 0; y < 4; y++)
							A[x][y] = Back[x][y];
				}
			}
		}
		else{
			if(currentMax < 48){
				for(int n = 1; n <=3 ;n ++){
					A[emptyX[i]][emptyY[i]] = n;

					//score += evaluate(A) * 0.33;
					score += 0.33 * maxValue(depth - 1, 1-playerIndex, A, alpha, beta);
					for(int x = 0; x < 4;x++)
						for(int y = 0; y < 4; y++)
							A[x][y] = Back[x][y];
				}
			}
				
			
			else{
				int num = currentMax/8 + 3;
				for(int n = 1; n <=3 ;n ++){
					A[emptyX[i]][emptyY[i]] = n;

					//score += evaluate(A) * 1.0 / num;
					score += (1.0/num) * maxValue(depth - 1, 1-playerIndex, A, alpha, beta);
					for(int x = 0; x < 4;x++)
						for(int y = 0; y < 4; y++)
							A[x][y] = Back[x][y];
				}
				
				for(int n = 1; n <= currentMax / 48; n++){
					//score += evaluate(A) * 1.0 / num;
					A[emptyX[i]][emptyY[i]] = n*6;
					score += (1.0/num) * maxValue(depth - 1, 1-playerIndex, A, alpha, beta);
					for(int x = 0; x < 4;x++)
						for(int y = 0; y < 4; y++)
							A[x][y] = Back[x][y];
				}
			}
		}
		v = std::min(v, score);
		if(alpha >= v)
			return v;
		beta = std::min(beta, v);

		for(int x = 0; x < 4;x++)
			for(int y = 0; y < 4; y++)
				A[x][y] = Back[x][y];
        
    }

    return v;
	
}


dir_e getAction(Grid grid, char hint){
	int Back[4][4] = {{0}}; // back up the array
	int legalActions[4] = {0};
	int newLocation[4][4] = {{0}};
	int currentMax = 0;
	int maxAction = 0;
	 
	double val[4] = {0};
	for(int i = 0; i < 4;i++)
		val[i] = -10000000;

	int **A;
	A = new int *[4];
	for(int i = 0; i <4; i++)
		A[i] = new int[4];

	for(int i = 0; i <= 15; i++){
		A[i/4][i%4] = Back[i/4][i%4] = grid[i];
		if(grid[i] > currentMax)
			currentMax = grid[i];
	}
	
	int legalAction = 0;
	
	int c = 0;
    for(int action = 0; action < 4; action++){
        if(isLegal(A, action)){
			legalActions[action] = 1;
			legalAction = action;
			c++;
		}
	}

	for(int action = 0; action < 4; action++){
		
		int max = 0;
		if(!legalActions[action]) continue;

		shift(A, action);
		
		
		if(isWin(A,currentMax)){
			maxAction = action;		
			break;
		}
		
		if(action == LEFT){
			for(int i = 0; i < 4;i++)
				for(int j = 0; j < 4; j++)
					if(A[i][j] != Back[i][j])
						if(A[i][3] == 0)
							newLocation[i][3] = 1;
		}
		
		if(action == RIGHT){
			for(int i = 0; i < 4;i++)
				for(int j = 0; j < 4; j++)
					if(A[i][j] != Back[i][j])
						if(A[i][0] == 0)
							newLocation[i][0] = 1;
		}
		
		if(action == UP){
			for(int i = 0; i < 4;i++)
				for(int j = 0; j < 4; j++)
					if(A[i][j] != Back[i][j])
						if(A[3][j] == 0)
							newLocation[3][j] = 1;
		}
		
		if(action == DOWN){
			for(int i = 0; i < 4;i++)
				for(int j = 0; j < 4; j++)
					if(A[i][j] != Back[i][j])
						if(A[0][j] == 0)
							newLocation[0][j] = 1;
		}
		
					
		
		double v ;
		double alpha = -INF;
        double beta = INF;
		
		if(currentMax > 384)
			v = minValue(MAX_DEPTH - 2 , 1, A, alpha, beta,newLocation,hint);

		if(currentMax > 192)
			v = minValue(MAX_DEPTH - 1 , 1, A, alpha, beta,newLocation,hint);

		else
			v = minValue(MAX_DEPTH  , 1, A, alpha, beta,newLocation,hint);
			
        val[action] = v;
        for(int i = 0; i <= 15; i++) A[i/4][i%4] =  grid[i];
        
        for(int i = 0; i < 4;i++)
			for(int j = 0; j < 4; j++){
				A[i][j] = Back[i][j];
				newLocation[i][j] = 0;
		}
	}
    
    double m = val[0];
   
    
    for(int i = 1; i < 4;i++){
		
		if(val[i] > m){
			m = val[i];
			maxAction = i;
		}
	}

	if(maxAction == 0)  return LEFT;
    if(maxAction == 1)  return DOWN;
    if(maxAction == 2)  return RIGHT;
    if(maxAction == 3)  return UP;
}
void PlayNRounds(int n){
#ifdef _WIN32
	system("cls");
#elif defined(__linux__)
	system("clear");
#endif
	int score;
	Game myGame;
	bool isGameOver;
	dir_e dir;

	gotoXY(5,0);
	std::cout<<"Previous";
	gotoXY(35,0);
	std::cout<<"Current (Hint: "<<myGame.getHint()<<")";
	myGame.printGrid(35,2);

	if(myGame.isGameOver(score))  myGame.reset();

	Grid myGrid;
	for(int i = 0;i < n;i++){    
		isGameOver = false;
		while(!isGameOver){
		
			gotoXY(5,10);
			myGame.printGrid(5,2);

			myGame.getCurrentGrid(myGrid);
			dir_e x = getAction(myGrid, myGame.getHint());
			cout << x << endl;
			myGame.insertDirection(x);
			gotoXY(50,0);
			std::cout<<myGame.getHint();
			isGameOver = myGame.isGameOver(score);
			myGame.printGrid(35,2);

		}
		myGame.printGrid(35,2);
		if(i < n - 1)  myGame.reset();
		gotoXY(0,15); 
		printf("  Round:    %d      \n", i+1);
		printf("  Score:    %d      \n", score);

	}
}

int main(int argc, char* argv[]){
	// Note: API function calls performed by any 'Game' object effects the same set of static class members,
	// so even through the 2 following function calls use different 'Game' objects, the same game continues
	PlayNRounds(100);
	//PlayNRounds(50);
	return 0;
}

